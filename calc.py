import argparse

parser = argparse.ArgumentParser(description="Perform arithmetic operations")
parser.add_argument("operand_1", type=float, help="first operand")
parser.add_argument("operator", type=str, help="operator (math sign)")
parser.add_argument("operand_2", type=float, help="second operand")
parser.add_argument(
    "--sentence", type=str, help="greeting sentence", default="Have a nice day!"
)
parser.add_argument("--name", type=str, help="name", required=True)


def calculate(args):
    math_operations = {
        "+": lambda op_1, op_2: op_1 + op_2,
        "-": lambda op_1, op_2: op_1 - op_2,
        "*": lambda op_1, op_2: op_1 * op_2,
        "/": lambda op_1, op_2: op_1 / op_2,
    }
    if operation := math_operations.get(args.operator):
        return operation(args.operand_1, args.operand_2)
    raise NotImplementedError(f"passed operation {args.operator} not implemented!")


def main():
    args = parser.parse_args()
    print(calculate(args))
    print(f"{args.sentence} {args.name}!")


if __name__ == "__main__":
    main()
