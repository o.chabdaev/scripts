class Greeting:
    def __init__(self, name):
        self.name = name
        self.text = "Hello"

    @staticmethod
    def decorator(func):
        def wrapper(*args, **kwargs):
            self = args[0]
            print(self.text)
            func(*args, **kwargs)
            print('End of work')
            return

        return wrapper

    @decorator
    def print_name(self):
        print(self.name)


class Greeting2(Greeting):
    def __init__(self, name):
        super().__init__(name)

    @Greeting.decorator
    def print_name_2(self):
        print(self.name)


person1 = Greeting2("Alex")
person1.print_name()
