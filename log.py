import logging
import sys
from logging import Formatter, StreamHandler


def get_logger(name):
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    handler = StreamHandler(stream=sys.stdout)
    LOGGING_FORMAT = f'[level=%(levelname)s (%(name)s:%(funcName)s:%(lineno)d)]: %(message)s'
    handler.setFormatter(Formatter(fmt=LOGGING_FORMAT))
    logger.addHandler(handler)
    return logger
