from log import get_logger

logger = get_logger(__name__)


class Animal:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def info(self):
        print(f"I am a {self.__class__.__name__.lower()}. My name is {self.name}. I am {self.age} years old.")


class Cat(Animal):

    def make_sound(self):
        logger.info("Meow")


class Dog(Animal):

    def make_sound(self):
        logger.info("Bark")


def main():
    cat1 = Cat("Kitty", 2.5)
    dog1 = Dog("Fluffy", 4)

    for animal in (cat1, dog1):
        animal.make_sound()
        animal.info()
        animal.make_sound()


if __name__ == '__main__':
    main()
