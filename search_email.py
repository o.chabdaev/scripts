import re


def email_search(text):
    email = re.findall(r"[\w.+-]+@[\w-]+\.[\w.-]{2,}", text)
    return email


print(
    email_search("Hello! Write me please to  chabdaev@ukr.net or o.chabdaev@gmail.com")
)
