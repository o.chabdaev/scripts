a = 10
b = 4

print("a & b =", a & b)
print("a | b =", a | b)
print("~a =", ~a)
print("a ^ b =", a ^ b)

a = 10
b = -10

print("a >> 1 =", a >> 1)
print("b >> 1 =", b >> 1)

a = 5
b = -10

print("a << 1 =", a << 1)
print("b << 1 =", b << 1)
