class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __hash__(self):
        # print(f"The hash of {self.name} aged {self.age} is:")
        return hash((self.name, self.age))

    def __eq__(self, other):
        return isinstance(other, Person) and hash(self) == hash(other)


person_1 = Person("Alex", 38)
person_2 = Person("Alex", 40)
person_3 = Person("Alex", 38)
person_4 = Person("Alex", 40)

# people = {hash(person_1), hash(person_2), hash(person_3), hash(person_4)}
people = {person_1, person_2, person_3, person_4}
print(people)