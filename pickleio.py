import pickle

data = {
    "a": [1, 2.0, 3, 4 + 6j],
    "b": ("character string", b"byte string"),
    "c": {None, True, False},
}
with open("data.pickle", "wb") as file:
    pickle.dump(data, file)
with open("data.pickle", "rb") as file:
    data_new = pickle.load(file)
print(data_new)
